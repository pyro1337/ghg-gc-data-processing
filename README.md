# README #

This is a script to process static GHG chamber measurements from a GC. It is specifically designed for the setup at SAE (www.sae.ethz.ch), but can be adjusted for other systems too (especially step 1 - data extraction).

### What is this repository for? ###

* Smooth datastream from GC to flux
* Well documented calibration
* Transparent data processing
* Reproducible results
* Graphical quality checks
* GHG flux calculation (gasfluxes R package integrated: https://bitbucket.org/ecoRoland/gasfluxes)

### How do I get set up? ###

* Download the script files; step 1 - 3
* Download the input file (.xlsx)
* Run each step in R
* how to run (packaged needed):

```
#!r

library(readxl)
library(data.table)
library(gasfluxes)  

source('~/ghg-gc-data-processing/SAE_step1_extraction.R')
```

etc...

### Who do I talk to? ###

* Roman Hüppi: roman.hueppi@usys.ethz.ch

### Dataflow Overview ###

![GC-Data-Processing](https://bitbucket.org/pyro1337/ghg-gc-data-processing/src/c16957ba753346bdb399b62f6601ea5f90a2fbde/170627_GC-DataProcessing_R_Manual.pdf "GC-Process-Overview")
see manual PDF